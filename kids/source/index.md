# Vítejte u Mondoshawanů 

Provozujeme v [Holubníku v Prusinách u Nebílov](http://holubnikprusiny.cz) malý dětský klub pro předškoláky. Pokračujeme v odkazu našich předchůdců Jeffa a Debi, kteří skupinu vedli v Plzni jako Malou anglickou školku. V Holubníku se mimo jiné inspirujeme zdravou výživou, volnějším režimem dětských aktivit a pobytem na čerstvém vzduchu.

Jedním z hlavních záměrů klubu je zlepšení konverzačních schopností dítěte v angličtině. Děti mohou ale nemusí být angličtině vyučovány v tradičním smyslu slova. Prioritou je zahloubání se do angličtiny tak aby se postupně stala přirozeností dítěte. Klub toho dosahuje především výlučným používáním angličtiny při aktivitách kde je třeba komunikovat a které vede rodilý mluvčí. Děti jsou po většinu času v kontaktu s rodilým mluvčím, ostatní pracovníci také hovoří anglicky a děti jsou povzbuzovány k používání angličtiny i mezi sebou.

Je pro nás důležité aby děti vyrůstaly jako zodpovědní a sebevědomí kamarádi. S tím souvisí vyšší věk přijímaných dětí a způsob vedení klubu. Jednáme s dětmi jako s partnery ve vřelém rodinném prostředí, ale nepředstíráme, že jsou dospělí. Na to mají času dost :).

A copak jsou ti Mondoshawani? Je to hravý odkaz na ideál správňáků, kteří chrání život na zemi (a ve vesmíru vůbec). Inspirovali jsme se filmem Pátý Element, což je pro Jo jeden z nejoblíbenějších. K tomu jsme založili spolek. Zápis je ale stále ještě v jednání u rejstříkového soudu. Nu, jsme jen Lidé (Mondoshawané) a procesy soudní jsou záludné.

Prozatím máme dětí dost, ale můžete se k nám nechat zapsat do seznamu čekatelů - využijte [kontakt níže](#contact).

Mějte se blaze, Mondoshawani.
