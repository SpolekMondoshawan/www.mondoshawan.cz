---
title: Early Spring
lang: en
date: 2018-04-17 18:06:08
author: Bani
---

So what have we been up to during our first month in March, and what are we up to in April? <!-- more -->

# Spring out of the box!

Our opening kick started with great excitement from all of us. The children had great fun exploring our new indoor space, meeting the animals in [Holubnik](http://holubnikprusiny.cz/), and discovering some of the fantastic outdoor possibilities nearby. This month we also started the experimentation of a new vegetarian and nutritous diet based on organic food, as our first preference and when possible, or from local farmers as our second option.

Since everything was new for the children as well as the team, we explored different ways in our daily rhythm to find a harmonious balance that works for all. We looked for a rhythm that supports the children's play and natural curiosity, yet we also wanted to develop a respectful and caring environment for everyone, especially our youngest members. For us it is important to achieve a balance between time reserved for free play, while also having time for group activities which stimulate children's learning in the English language.

# March

Some more of our highlights in March included decorating a craft box for recycling materials using a super simple homemade glue that we made together; baked a cake from scratch for Angelica Joy; hugging sessions of our littlest members Angelica Joy and Richie; creating houses and beds in very original ways all thought of by the children's imagination; blowing out eggs and decorating them by paint afterwards;  colouring eggs (from Holubnik's chickens) using onion and turmeric as dye; and exploring the bushy maze in the forest nearby. Nature is our biggest inspiration and teacher so we are working together with its cycles to facilitate English learning. 

# April

During the month of April our current big topic is early Spring. We are discovering what happens during this time of the year through the changes in nature during our walks, and outdoor play. In our first week we explored the forest nearby and found the first flowers symbolizing the arrival of Spring (mainly the Snow drop) and this was the reason why we made some of these flowers during our craft time. We document what we find in nature in drawing on our 'Early Spring' chart which we will keep filling up as we go along the Season. The children were excited to have a fire during snack time and so this was a good opportunity to discuss fire safety. The children loved the outdoor picnics in the fresh air looking out for the birds in Spring and their fantastic singing. We also planted our first seeds and its quite exciting to see the little shoots grow in one of our classrooms.

# Routine

Our mornings usually start with the kids having so much fun in the simple activity of pouring tea and adding honey to it. During the second week in April the children discovered the Romanesco vegetable and loved eating it raw. We made up bug stories as we learned bug names and a little bit about their life. On Thursday we went on a Treasure walk where we collected some items and later had a chat on how they differ from each other and why or why are they the same - which is another topic that we will be working on during the next couple of weeks. On the way we also collected some trashy plastic to clean up the forest path; found real bugs this time and discussed that we should not disturb them while they are making babies :); and we also saw a centipede which was quite fascinating to observe. On Friday we discovered shells from the sea  and those from land, and the difference between a small stone and other items such as an onion. During the day we spent time outside where we planted fennel seeds, and bean seeds. In the afternoon some of the children had fun pony riding while the rest enjoyed digging the earth and made a little ditch for a massive river to run through.
