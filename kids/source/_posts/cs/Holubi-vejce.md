---
title: Holubí vejce
lang: cs
date: 2018-04-12 22:42:12
author: Jo
---

Je po Velikonocích a mě napadlo takové veselé přirovnání k holubímu vejci. Uhnízdili jsme se v [Holubníku](http://holubnikprusiny.cz) a snesli jsme zde vejce našeho nového projektu - dětského klubu "Shine your light". <!-- more --> Vejce obsahuje dobroty od Jeffa a Debi, a vyrůstá v novém prostoru komunitní školy a školky. Naše holubí vajíčko je nabité vděčností. Díky Jeffe, díky Debi, díky Evo, díky děti, díky rodičům, díky Bani, díky Ivo, díky Jo... haha, díky všem! Vděčnost je vydatný základ.

Je tu hezky. Jaro na nás neodbytně doléhá a láká k toulkám přírodou. Stromy pučí a kvetou, že jsme si to správně načasovali. Děti pečlivě zkoumají znamení, jimiž se jaro ohlašuje, a bez ostychu o nich anglicky debatují...

